var hospitalExpectedOrders = document.querySelector(
  ".hospital_expected_orders"
);
var addHospitalButton = document.querySelector(".add_hospital");
var hospitalList = document.querySelector("#hospitals-list");
var agentList = document.querySelector("#agents-list");
var hospitalAddForm = document.querySelector("#hospital-add");
var hosptalId, hospitalName, agentName;

// Remove Modal
document.querySelector(".add-hospital-item").addEventListener("click", () => {
  document.querySelector(".add-hospital").classList.add("show-main-modal");
});

// Remove Modal
window.addEventListener("click", e => {
  if (e.target.id == "hospital-add") {
    document.querySelector(".add-hospital").classList.remove("show-main-modal");
  }
});

// Add Hospital
addHospitalButton.addEventListener("click", e => {
  e.preventDefault();
  hospitalExpectedOrders = hospitalExpectedOrders.value;
  // console(
  //   `hos_id=${hosptalId}&amount=${hospitalExpectedOrders}&user=${
  //     agentList.value
  //   }`
  // );
  // POST request
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector(".loader-container").classList.add("show-loader");
    } else if (this.readyState == 4 && this.status == 200) {
      alert(this.responseText);
      // console(this.responseText);
      // document.querySelector('#edit-modal').classList.remove('show-main-modal');
      document.location.reload();
    }
  };
  xhttp.open("POST", "https://lifebank.ng/crm/new/api/expect/add", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(
    `hos_id=${hosptalId}&amount=${hospitalExpectedOrders}&user=${
      agentList.value
    }`
  );
});

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
    document.querySelector(".loader-container").classList.add("show-loader");
  } else if (this.readyState == 4 && this.status == 200) {
    var resData = JSON.parse(xhttp.responseText);
    // var resData = JSON.parse(xhttp.responseText).sort((a, b) =>
    //   a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1
    // );
    // console(resData);
    document.querySelector(".loader-container").classList.remove("show-loader");
    //  Build HTML
    hospitalList.innerHTML =
      '<option value="">Choose Hospital</option>' +
      resData
        .map(data => {
          return `<option value="${data.name}">${data.name}</option>`;
        })
        .join("");

    hospitalList.addEventListener("change", e => {
      hospitalName = hospitalList.value.trim();
      resData.filter(data => {
        if (hospitalName == data.name) {
          hosptalId = data.Hospital_ID;
        }
        validateQueryForm();
      });
    });
  }
};
xhttp.open("GET", "https://lifebank.ng/crm/new/api/hospital/getlist", true);
xhttp.send();

var xhttpAgent = new XMLHttpRequest();
xhttpAgent.onreadystatechange = function() {
  if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
    document.querySelector(".loader-container").classList.add("show-loader");
  } else if (this.readyState == 4 && this.status == 200) {
    var resData = JSON.parse(xhttpAgent.responseText).sort((a, b) =>
      a.username.toUpperCase() > b.username.toUpperCase() ? 1 : -1
    );
    // var resData = JSON.parse(xhttpAgent.responseText);
    // console(resData);
    document.querySelector(".loader-container").classList.remove("show-loader");
    //  Build HTML
    agentList.innerHTML =
      '<option value="">Assign Agent</option>' +
      resData
        .map(data => {
          return `<option value="${
            data.id
          }">${data.username.toUpperCase()}</option>`;
        })
        .join("");

    agentList.addEventListener("change", e => {
      agentName = agentList.value.trim();
      validateQueryForm();
    });
  }
};
xhttpAgent.open("GET", "https://lifebank.ng/crm/new/api/user/getlist/all", true);
xhttpAgent.send();

function validateQueryForm() {
  if (
    hospitalExpectedOrders.value.trim().length == 0 ||
    hospitalName.length == 0 ||
    agentList.value.length == 0
  ) {
    addHospitalButton.setAttribute("disabled", "");
    addHospitalButton.classList.add("disabled-botton");
  } else {
    addHospitalButton.removeAttribute("disabled");
    addHospitalButton.classList.remove("disabled-botton");
  }
}

hospitalAddForm.addEventListener("keyup", () => {
  validateQueryForm();
});

hospitalExpectedOrders.addEventListener("input", validateQueryForm);

validateQueryForm();
