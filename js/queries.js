var url = localStorage.getItem('url');

var auth_details = JSON.parse(localStorage.getItem('crmAuth'));
var submitButton = document.querySelector('.submit-issue');

var resolveContent = document.querySelector('#resolved-content');
var queryId;

if (
  localStorage.getItem('crmAuth') &&
  (auth_details.class != 'CustomerCare' || auth_details.class == 'All')
) {
  document.querySelector(
    '.login-session'
  ).innerHTML = `<span class="complaint-notification">1</span> ${
    auth_details.username
  } <i class="far fa-user"></i> <i class="fas fas fa-ellipsis-v"></i>`;

  document.querySelector('.user-nav-info h5').innerText = auth_details.username;
  document.querySelector('.user-nav-info p').innerText = auth_details.email;

  // Get queries request
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      document
        .querySelector('.loader-container')
        .classList.remove('show-loader');
      var resData = JSON.parse(xhttp.responseText);
      // console(resData);
      if (resData == 'No Issues') {
        document.querySelector(
          '#queries-hold'
        ).innerHTML = `<h3>${resData}</h3>`;
        return;
      }

      var queries = resData
        .sort((a, b) => (a.id > b.id ? -1 : 1))
        .map(query => {
          return `
        <div class="card w-75">
          <div class="card-body">
            <h5 class="card-title">${query.title}</h5>
            <p class="card-text">${query.issues}</p>
            <div class="action_btn_hold">
              <a href="#" class="btn action-btn" data-toggle="collapse" data-target="#collapseExample${
                query.id
              }" aria-expanded="false" aria-controls="collapseExample">Take Action</a>
            </div>
          </div>
            
            <div class="collapse more_query" id="collapseExample${query.id}">
              <h5>Suggested actions:</h5>
              <p>${showSteps(query.title)}</p>
              <div class="action_btn_hold">
                <a id=${
                  query.id
                } href="#" class="btn clear-issues action-btn">Resolve Issue</a>
              </div>
            </div>
          </div>
        </div>
        `;
        });
      document.querySelector('#queries-hold').innerHTML =
        '<h2>Queries</h2>' + queries.join(' ');

      // console(document.querySelectorAll('.clear-issues'));

      document.querySelectorAll('.clear-issues').forEach(clearIssue => {
        clearIssue.addEventListener('click', e => {
          document
            .querySelector('.confirm-modal')
            .classList.add('show-confirm-modal');
          queryId = e.target.id;
        });
      });
    }
  };
  xhttp.open(
    'GET',
    `https://lifebank.ng/crm/new/api/issues/${auth_details.class}`,
    true
  );
  xhttp.send();

  document.querySelector('#drop-down-hold').addEventListener('click', () => {
    document.querySelector('.overlay').style.display = 'block';
    setTimeout(() => {
      document.querySelector('nav').style.transform = 'translateX(0%)';
    }, 100);
  });

  document.querySelector('.close-nav').addEventListener('click', () => {
    document.querySelector('.overlay').style.display = 'none';
    setTimeout(() => {
      document.querySelector('nav').style.transform = 'translateX(100%)';
    }, 100);
  });

  // ***************Navigation links **************************//
  //
  document.querySelector('.complaint-link').addEventListener('click', e => {
    e.preventDefault();
    window.location.href = './complaint-check.html';
  });

  document.querySelector('.change-password').addEventListener('click', e => {
    e.preventDefault();
    window.location.href = './update-password.html';
  });

  // Logout Action
  document.querySelector('.logout-item').addEventListener('click', () => {
    localStorage.removeItem('crmAuth');
    window.location.replace(`${url}`);
  });
} else {
  window.location.replace(`${url}`);
}

// Resolve Query
document.querySelector('.confirm-modal button').addEventListener('click', e => {
  e.preventDefault();
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      var res = JSON.parse(xhttp.responseText);
      document.location.reload();
    }
  };
  xhttp.open('POST', 'https://lifebank.ng/crm/new/api/issues/resolved', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send(`id=${queryId}&content=${resolveContent.value}`);
});

// Categories of different queries
function showSteps(queryTitle) {
  switch (queryTitle) {
    case 'Price':
      return `<ul>
        <li>For Silver ⇢ Offer Bronze (lowest price) with volume ⇢ no volume ⇢ intermittent list.</li>
        <li>For Gold ⇢ Offer Silver officially ⇢ Still not ordering ⇢ Offer Bronze with volume ⇢ No volume ⇢ Intermittent list.</li>
        <li>Platinum ⇢ Offer Gold officially ⇢ (No order) ⇢ Offer Silver ⇢ (No order) ⇢ Offer Bronze (with volume). No volume ⇢ Intermittent list.</li>
      </ul>`;

    case 'Time delay':
      return `<ul>
        <li>Find out history and validate.</li>
        <li>Call hospital, apologize and reassure.</li>
        <li>Offer a discount on next order (5% up to #5000)</li>
        <li>Failed; communicate sales to schedule a visit.</li>
        <li>Offer discount</li>
      </ul>`;

    case 'Unfulfilled orders':
      return `<ul>
        <li>Put a call to the hospital to validate.</li>
        <li>If Internal problem, closen issue.</li>
        <li>If Lifebank, re-route to reasons.</li>
        <li>Follow processes and close.</li>
      </ul>`;

    case 'Invoice error':
      return `<ul>
        <li>New bills get one invoice with details on order.</li>
        <li>Old bills get one invoice with detailed information regarding the old bill.</li>
      </ul>`;

    case 'Bad blood':
      return `<ul>
        <li>Call to acknowledge and manage the situation.</li>
      </ul>`;
    case 'Debt':
      return `<ul>
        <li>Finance offers instant payment options.</li>
        <li> Tags account as instant payment</li>
      </ul>`;

    case 'Gifts Rewards':
      return `<ul>
        <li>Calls to acknowledge.</li>
        <li>Happy, Do Nothing.</li>
        <li>Unhappy, promise gift at the next festive season.</li>
        <li>Send an appropriate gift.</li>
      </ul>`;

    case 'Rude Riders':
      return `<ul>
        <li>Call to validate.</li>
        <li>Apologize.</li>
        <li>Sends a rider to the hospital to apologize.</li>
      </ul>`;

    case 'Lost contact':
      return `<ul>
        <li>Send Lifebank Contact As SMS To The Customer.</li>
        <li>Also sends a dispatch with a wall sticker to the hospital.</li>
      </ul>`;

    case 'Change in hospital personnel':
      return `<ul>
        <li>Send Lifebank Contact As SMS To The Customer.</li>
        <li>Call to validate.</li>
        <li>Revisits.</li>
        <li>Sales process restarts (Pipeline).</li>
      </ul>`;

    case 'Low cause of switching':
      return `<ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>`;

    case 'Previous commitment':
      return `<ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>`;

    case 'Rude Slow Customer Service(Sense of urgency)':
      return `<ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>`;

    case 'Market Downturn':
      return `<ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>`;
  }
}

document.querySelector('textarea').addEventListener('keyup', () => {
  validateQueryForm();
});

// Validate Form
function validateQueryForm() {
  if (document.querySelector('textarea').value.trim().length == 0) {
    submitButton.setAttribute('disabled', '');
    submitButton.classList.add('disabled-botton');
  } else {
    submitButton.removeAttribute('disabled');
    submitButton.classList.remove('disabled-botton');
  }
}

// Clear dialog for update
window.addEventListener('click', e => {
  if (e.target.id == 'confirm-dialog') {
    document
      .querySelector('#confirm-dialog')
      .classList.remove('show-confirm-modal');
  }
});

validateQueryForm();
