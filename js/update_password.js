let url = localStorage.getItem('url');

const pwd = document.querySelector('#pwd');
const form = document.querySelector('form');
const pwconfirmPwd = document.querySelector('#confirm-pwd');
const submitBtn = document.querySelector('button');

const authUser = JSON.parse(localStorage.getItem('crmAuth'));

if (!authUser) window.location.replace(`${url}`);

function validateForm() {
  if (
    pwd.value.trim() === pwconfirmPwd.value.trim() &&
    pwd.value.trim().length > 0
  ) {
    submitBtn.style.backgroundColor = '#930505';
    submitBtn.style.cursor = 'pointer';
    submitBtn.removeAttribute('disabled', '');
  } else {
    submitBtn.style.backgroundColor = '#525252';
    submitBtn.style.cursor = 'auto';
    submitBtn.setAttribute('disabled', '');
  }
}

function updatePwd(e) {
  e.preventDefault();

  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      resData = JSON.parse(xhttp.responseText);
      if (resData == 'Password Updated') {
        alert('Password updated. \nPlease login again.');
        localStorage.removeItem('crmAuth');
        window.location.replace(`${url}`);
      }
    }
  };
  xhttp.open('POST', 'http://crm.lifebank.ng/new/api/user/update', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send(`id=${authUser.id}&password=${pwd.value.trim()}`);
}

pwd.addEventListener('keyup', validateForm);
pwconfirmPwd.addEventListener('keyup', validateForm);
submitBtn.addEventListener('click', updatePwd);
form.addEventListener('submit', updatePwd);

validateForm();
