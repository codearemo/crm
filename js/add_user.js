var input_username = document.querySelector('#input_username');
var input_password = document.querySelector('#input_password');
var input_email = document.querySelector('#input_email');
var submitButton = document.querySelector('.add-new-user');
var addUserForm = document.querySelector('.add-user');

var dept = document.querySelector('#dept_categories');

document.querySelector('.add-user-item').addEventListener('click', () => {
  document.querySelector('.add-user').classList.add('show-main-modal');
});

// Clear query off screen
document.querySelector('#cancel-btn').addEventListener('click', e => {
  document.querySelector('.main-modal').classList.remove('show-main-modal');
});

window.addEventListener('click', e => {
  if (e.target.id == 'user-add') {
    document.querySelector('.add-user').classList.remove('show-main-modal');
  }
});

// Submit Form for adding new user
submitButton.addEventListener('click', e => {
  e.preventDefault();
  // Get the input
  var email = document.querySelector('#input_email');
  var password = document.querySelector('#input_password');

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      resData = JSON.parse(xhttp.responseText);
      if (resData == 'user created') {
        alert(`New ${dept.value} account created`);
        document.querySelector('.add-user').classList.remove('show-main-modal');
        window.location.reload();
      } else {
        alert(resData);
        document
          .querySelector('.loader-container')
          .classList.remove('show-loader');
      }
    }
  };
  xhttp.open('POST', 'https://lifebank.ng/crm/new/api/user/create', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send(
    `class=${dept.value}&username=${input_username.value}&email=${
      email.value
    }&password=${password.value}`
  );
});

// Validate form
function validateQueryForm() {
  if (
    input_email.value.trim().length == 0 ||
    dept.value.trim().length == 0 ||
    input_username.value.length == 0
  ) {
    submitButton.setAttribute('disabled', '');
    submitButton.classList.add('disabled-botton');
  } else {
    alert();
    submitButton.removeAttribute('disabled');
    submitButton.classList.remove('disabled-botton');
  }
}

addUserForm.addEventListener('keyup', () => {
  if (
    input_email.value.trim().length == 0 ||
    dept.value.trim().length == 0 ||
    input_username.value.length == 0
  ) {
    submitButton.setAttribute('disabled', '');
    submitButton.classList.add('disabled-botton');
  } else {
    submitButton.removeAttribute('disabled');
    submitButton.classList.remove('disabled-botton');
  }
});

// Select Dept Action
dept.addEventListener('change', () => {
  if (
    input_email.value.trim().length == 0 ||
    dept.value.trim().length == 0 ||
    input_username.value.length == 0
  ) {
    submitButton.setAttribute('disabled', '');
    submitButton.classList.add('disabled-botton');
  } else {
    submitButton.removeAttribute('disabled');
    submitButton.classList.remove('disabled-botton');
  }
});

validateQueryForm();
