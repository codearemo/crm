let hospitalList,
  complaintHospitalId = '';
const backBtn = document.querySelector('.back-icon');
const populatedList = document.querySelector('.hospital-list');
const hospitalNameInput = document.querySelector('#hospital-name');
const contactNumber = document.querySelector('#phone');
const contactName = document.querySelector('#contact-name');
const complaintDate = document.querySelector('#complaint-date');
const compliant = document.querySelector('textarea');
const dept = document.querySelector('#dept');
const submitForm = document.querySelector('form');
const submitBtn = document.querySelector('.send-issue');

const authUser = JSON.parse(localStorage.getItem('crmAuth'));
let url = localStorage.getItem('url');

if (!authUser) window.location.replace(`${url}`);

function submitComplaint(e) {
  e.preventDefault();

  // console.log(complaintHospitalId);
  // console.log(contactNumber.value.trim());
  // console.log(complaintDate.value.trim());
  // console.log(compliant.value.trim());
  // console.log(contactName.value.trim());
  // console.log(dept.value.trim());

  if (
    complaintHospitalId.length == 0 ||
    contactNumber.value.trim().length == 0 ||
    complaintDate.value.trim().length == 0 ||
    compliant.value.trim().length == 0 ||
    contactName.value.trim().length == 0 ||
    dept.value.trim().length == 0
  ) {
    alert('Please check that all fields were properly filled.');
    return;
  }

  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      document
        .querySelector('.loader-container')
        .classList.remove('show-loader');
      resData = JSON.parse(xhttp.responseText);
      if (resData == 'Record Added successfully') {
        alert(resData);
        window.location.replace(`${url}dashboard.html`);
      } else {
        alert('An Error occured. \nPlease try again.');
      }
    }
  };
  xhttp.open('POST', 'http://crm.lifebank.ng/new/api/complaint/log', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send(
    `Hospital_ID=${complaintHospitalId}&compliant=${compliant.value.trim()}&contact_number=${contactNumber.value.trim()}&incident_time=${
      complaintDate.value
    }&cs_id=${
      authUser.id
    }&dept=${issueDeptSubmission}&contact_person=${contactName.value.trim()}`
  );
}

// Fetch all hospitals
fetch('https://lifebank.ng/crm/new/api/hospital/getlist')
  .then(res => res.json())
  .then(data => {
    hospitalList = data;

    if (Array.isArray(hospitalList) && hospitalList.length > 0) {
      // Typing action
      hospitalNameInput.addEventListener('keyup', e => {
        const textContent = e.target.value;
        if (textContent.length == 0) {
          populatedList.style.display = 'none';
          return;
        }
        populatedList.style.display = 'block';
        const filteredList = hospitalList.filter(hospital =>
          hospital.name.toLowerCase().includes(textContent.toLowerCase())
        );
        // Populate list
        populatedList.innerHTML = filteredList
          .map(
            filteredItem =>
              `<p class="filtered-hopitals" data-hosId="${
                filteredItem.Hospital_ID
              }">${filteredItem.name}</p>`
          )
          .join('');

        // Select list
        document.querySelectorAll('.filtered-hopitals').forEach(hospital => {
          hospital.addEventListener('click', e => {
            complaintHospitalId = e.target.dataset.hosid;
            hospitalNameInput.value = e.target.innerText;
          });
        });
      });
    }
  });

dept.addEventListener('change', () => {
  issueTitleSubmission = dept.value;
  switch (issueTitleSubmission) {
    case 'Price':
      issueDeptSubmission = 'sales';
      break;
    case 'Time delay':
      issueDeptSubmission = 'operations';
      break;
    case 'Unfulfilled orders':
      issueDeptSubmission = 'operations';
      break;
    case 'Invoice error':
      issueDeptSubmission = 'finance';
      break;
    case 'Bad blood':
      issueDeptSubmission = 'operations';
      break;
    case 'Debt':
      issueDeptSubmission = 'finance';
      break;
    case 'Gifts Rewards':
      issueDeptSubmission = 'operations';
      break;
    case 'Rude Riders':
      issueDeptSubmission = 'operations';
      break;
    case 'Lost contact':
      issueDeptSubmission = 'operations';
      break;
    case 'Change in hospital personnel':
      issueDeptSubmission = 'sales';
      break;
    case 'Low cause of switching':
      issueDeptSubmission = 'operations';
      break;
    case 'Previous commitment':
      issueDeptSubmission = 'sales';
      break;
    case 'Rude Slow Customer Service(Sense of urgency)':
      issueDeptSubmission = 'operations';
      break;
    case 'Market Downturn':
      issueDeptSubmission = 'sales';
      break;
    default:
      issueDeptSubmission = '';
  }
});

backBtn.addEventListener('click', () => window.history.back()());

// Submit issue
submitBtn.addEventListener('click', submitComplaint);
submitForm.addEventListener('submit', submitComplaint);

// Check if clicked area was not part of the pop up hospitals list
window.addEventListener('click', e => {
  if (e.target.id != 'hospital-name') {
    populatedList.style.display = 'none';
  } else {
    populatedList.style.display = 'block';
  }
});
