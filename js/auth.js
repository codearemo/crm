var url = localStorage.getItem('url');

var resData;
var submitButton = document.querySelector('#auth_submit');

// Check if already logged in
if (localStorage.getItem('crmAuth')) {
  var authUser = JSON.parse(localStorage.getItem('crmAuth'));
  switch (authUser.class) {
    case 'tech' || 'admin':
      window.location.replace(`${url}admin.html`);
      break;
    case 'CustomerCare':
      window.location.replace(`${url}dashboard.html`);
      break;
    default:
      window.location.replace(`${url}queries.html`);
      break;
  }
}

if (submitButton != null) {
  submitButton.addEventListener('click', e => {
    e.preventDefault();

    // Get the input
    var email = document.querySelector('#auth_email').value;
    var password = document.querySelector('#auth_password').value;


    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
        document.querySelector('.loader-container').classList.add('show-loader');
      }
      else if (this.readyState == 4 && this.status == 200) {
        resData = JSON.parse(xhttp.responseText);
        if (resData == 'User with that email doesn\'t exist!' || resData == 'Incorrect Password!') {
          alert('Incorrect Username or Password');
          document.querySelector('.loader-container').classList.remove('show-loader');
        }
        else {
          var auth_resData = JSON.stringify(resData);
          localStorage.setItem('crmAuth', auth_resData);
          if (resData.class == 'CustomerCare') {
            window.location.replace(`${url}dashboard.html`);
          } else if (resData.class == 'tech' || resData.class == 'admin') {
            window.location.replace(`${url}admin.html`);
          } else {
            window.location.replace(`${url}queries.html`);
          }
        }
      }
    };
    xhttp.open("POST", "https://lifebank.ng/crm/new/api/login", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(`email=${email}&password=${password}`);
  });
};