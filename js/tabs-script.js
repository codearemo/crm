var auth_details = JSON.parse(localStorage.getItem('crmAuth'));

// Code to tab bar

// Get DOM items
const tabs = document.querySelectorAll('.nav-tabs li');
const contents = document.querySelectorAll('.tab-content > div');

if (auth_details.class == 'CustomerCare' || auth_details.class == 'All') {

  function showTab(e, route) {
    tabs.forEach(tab => {
      if (e.target.dataset.toggle == tab.childNodes[0].id || tab.childNodes[0].id == route) {
        tab.classList.add('active');
      } else {
        tab.classList.remove('active');
      }
    })
    contents.forEach(content => {
      if (e.target.dataset.toggle == content.id || content.id == route) {
        content.classList.add('active');
      } else {
        content.classList.remove('active');
      }
    })
  }

  document.querySelector('#view1').addEventListener('click', e => {
    showTab(e, 'activity');
  });

  document.querySelector('#view2').addEventListener('click', e => {
    showTab(e, 'hospitals');
  });



  document.querySelector('.card ul').addEventListener('click', showTab);
}
