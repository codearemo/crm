var url = localStorage.getItem('url');

var auth_details = JSON.parse(localStorage.getItem('crmAuth'));
var submitButton = document.querySelector('.submit-issue');

var resolveContent = document.querySelector('#resolved-content');
var queryId;

if (
  localStorage.getItem('crmAuth') &&
  (auth_details.class != 'CustomerCare' || auth_details.class == 'All')
) {
  // Get queries request
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      document
        .querySelector('.loader-container')
        .classList.remove('show-loader');
      let resData = JSON.parse(xhttp.responseText);

      console.log(resData);

      document.querySelector('.login-session').innerHTML = Array.isArray(
        resData
      )
        ? `<span class="complaint-notification">${resData.length}</span> ${
            auth_details.username
          } <i class="far fa-user"></i> <i class="fas fas fa-ellipsis-v"></i>`
        : `${
            auth_details.username
          } <i class="far fa-user"></i> <i class="fas fas fa-ellipsis-v"></i>`;

      document.querySelector('.user-nav-info h5').innerText =
        auth_details.username;
      document.querySelector('.user-nav-info p').innerText = auth_details.email;

      // console(resData);
      if (resData == 'No Complaint') {
        document.querySelector(
          '#complaint-hold'
        ).innerHTML = `<h3>${resData}</h3>`;
        return;
      }

      console.log(resData);

      let complaint = resData
        .sort((a, b) => (a.id > b.id ? -1 : 1))
        .map(complaint => {
          return `
        <div class="card w-75">
          <div class="card-body">
            <h5 class="card-title" style="font-size: 16px;">${
              complaint.name
            }</h5>
            <p style="margin-bottom: 5px">Contact Name:  ${
              complaint.contact_person
            }</p>
            <p style="margin-bottom: 5px">Contact Number:  ${
              complaint.contact_number
            }</p>
            <p style="margin-bottom: 5px">Date of Incident:  ${
              complaint.incident_time
            }</p>
            <div class="action_btn_hold">
              <a href="#" class="btn action-btn" data-toggle="collapse" data-target="#collapseExample${
                complaint.id
              }" aria-expanded="false" aria-controls="collapseExample" style="font-size: 13px;">Check Complaint</a>
            </div>
          </div>

            <div class="collapse more_complaint" id="collapseExample${
              complaint.id
            }">
              <h5>Complain</h5>
              <p>${complaint.compliant}</p>
              <div class="action_btn_hold">
                <a id=${
                  complaint.id
                } href="#" class="btn clear-issues action-btn">Fix Complaint</a>
              </div>
            </div>
          </div>
        </div>
        `;
        });
      document.querySelector('#complaint-hold').innerHTML =
        '<h2>Complains</h2>' + complaint.join(' ');

      // console.log(document.querySelectorAll('.clear-issues'));

      document.querySelectorAll('.clear-issues').forEach(clearIssue => {
        clearIssue.addEventListener('click', e => {
          document
            .querySelector('.confirm-modal')
            .classList.add('show-confirm-modal');
          queryId = e.target.id;
        });
      });
    }
  };
  xhttp.open(
    'GET',
    `https://lifebank.ng/crm/new/api/complaint/${auth_details.class}`,
    true
  );
  xhttp.send();

  document.querySelector('#drop-down-hold').addEventListener('click', () => {
    document.querySelector('.overlay').style.display = 'block';
    setTimeout(() => {
      document.querySelector('nav').style.transform = 'translateX(0%)';
    }, 100);
  });

  document.querySelector('.close-nav').addEventListener('click', () => {
    document.querySelector('.overlay').style.display = 'none';
    setTimeout(() => {
      document.querySelector('nav').style.transform = 'translateX(100%)';
    }, 100);
  });

  // ***************Navigation links **************************//
  //
  document.querySelector('.query-section').addEventListener('click', e => {
    e.preventDefault();
    window.location.href = './queries.html';
  });

  document.querySelector('.change-password').addEventListener('click', e => {
    e.preventDefault();
    window.location.href = './update-password.html';
  });

  // Logout Action
  document.querySelector('.logout-item').addEventListener('click', () => {
    localStorage.removeItem('crmAuth');
    window.location.replace(`${url}`);
  });
} else {
  window.location.replace(`${url}`);
}

// Resolve Complaint
document.querySelector('.confirm-modal button').addEventListener('click', e => {
  e.preventDefault();
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      var res = JSON.parse(xhttp.responseText);
      document.location.reload();
    }
  };
  xhttp.open(
    'POST',
    'https://lifebank.ng/crm/new/api/complaint/resloved',
    true
  );
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send(`complaint_id=${queryId}&feedback=${resolveContent.value.trim()}`);
});

document.querySelector('textarea').addEventListener('keyup', () => {
  validateQueryForm();
});

// Validate Form
function validateQueryForm() {
  if (document.querySelector('textarea').value.trim().length == 0) {
    submitButton.setAttribute('disabled', '');
    submitButton.classList.add('disabled-botton');
  } else {
    submitButton.removeAttribute('disabled');
    submitButton.classList.remove('disabled-botton');
  }
}

// Clear dialog for update
window.addEventListener('click', e => {
  if (e.target.id == 'confirm-dialog') {
    document
      .querySelector('#confirm-dialog')
      .classList.remove('show-confirm-modal');
  }
});

validateQueryForm();
