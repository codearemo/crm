var url = localStorage.getItem("url");

var auth_details = JSON.parse(localStorage.getItem("crmAuth"));
var paginationList = [];
var paginationQueries, asignedUser;

if (
  localStorage.getItem("crmAuth") &&
  (auth_details.class != "CustomerCare" || auth_details.class == "All")
) {
  document.querySelector(".login-session").innerHTML = `<p><em>${
    auth_details.username
  }</em> logout</p>`;
  document.querySelector(".user-nav-info h5").innerText = auth_details.username;
  document.querySelector(".user-nav-info p").innerText = auth_details.email;

  document.querySelector("#drop-down-hold").innerHTML = `${
    auth_details.username
  } <i class="far fa-user"></i> <i class="fas fas fa-ellipsis-v"></i>`;

  function populatePieChart(piechart) {
    return piechart.map(piechartSection => {
      return [piechartSection.title, parseInt(piechartSection.amount)];
    });
  }

  // Get queries request
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector(".loader-container").classList.add("show-loader");
    } else if (this.readyState == 4 && this.status == 200) {
    }
  };
  xhttp.open("GET", `https://lifebank.ng/crm/new/api/issues/admin/get`, true);
  xhttp.send();

  document.querySelector("#drop-down-hold").addEventListener("click", () => {
    document.querySelector(".overlay").style.display = "block";
    setTimeout(() => {
      document.querySelector("nav").style.transform = "translateX(0%)";
    }, 100);
  });

  document.querySelector(".close-nav").addEventListener("click", () => {
    document.querySelector(".overlay").style.display = "none";
    setTimeout(() => {
      document.querySelector("nav").style.transform = "translateX(100%)";
    }, 100);
  });

  document.querySelector(".logout-item").addEventListener("click", () => {
    localStorage.removeItem("crmAuth");
    window.location.replace(`${url}`);
  });
} else {
  window.location.replace(`${url}`);
}

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
    document.querySelector(".loader-container").classList.add("show-loader");
  } else if (this.readyState == 4 && this.status == 200) {
    document.querySelector(".loader-container").classList.remove("show-loader");

    var resData = JSON.parse(this.responseText);
    // console(resData);

    // Hospital Section
    var hospitalsSection = resData
      .sort((a, b) => (a.name > b.name ? 1 : -1))
      .map((hospital, index) => {
        return `
            <tr>
              <th scope="row">${index + 1}</th>
              <td style="display: flex; justify-content: flex-start; padding-left: 10vw">${hospital.name}</td>
              <td>${hospital.Expected}</td>
              <td> <button type="button" id="${
                hospital.id
              }" class="btn btn-primary btn-sm edit-expected-orders" data-assigned="${hospital['User ID']}">Edit</button></td>
            </tr>`;
      });

    // Fill the table
    document.querySelector("#table-section").innerHTML = `
        <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col" style="padding-left: 12vw">Hospitals</th>
        <th scope="col" style="text-align: center;">Expected Orders/month</th>
        <th scope="col" style="text-align: center;">Edit Expected Orders</th>
        </tr>
        ${hospitalsSection.join("")}
        </thead>
        <tbody>
        
        <caption>List of hospitals and their expected orders</caption>
        `;

    // Click edit button for for new order update
    document.querySelectorAll(".edit-expected-orders").forEach(hospitalEdit => {
      hospitalEdit.addEventListener("click", e => {
        document.querySelector("#edit-modal .alert-dialog h4 span").innerHTML =
          e.path[2].children[1].innerText;
        document.querySelector("#update_number").value =
          e.path[2].children[2].innerText;
        document.querySelector("#edit-modal").classList.add("show-main-modal");
        editExpectedOrdersId = e.target.id;
        // Set assigned user to hospital
        asignedUser= e.target.dataset.assigned;
        document.querySelector("#asigned-list").value = e.target.dataset.assigned;
      });
    });

    // Update Expected Amount of Orders
    document.querySelector("#updateBotton").addEventListener("click", e => {
      e.preventDefault();
      editExpectedOrdersValue = document.querySelector(".edit-form input")
      .value;
      // console(`id=${editExpectedOrdersId}&amount=${editExpectedOrdersValue}&user=${asignedUser}`);
      // POST request
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (
          this.readyState == 1 ||
          this.readyState == 2 ||
          this.readyState == 3
        ) {
          document
            .querySelector(".loader-container")
            .classList.add("show-loader");
        } else if (this.readyState == 4 && this.status == 200) {
          alert(this.responseText);
          document
            .querySelector("#edit-modal")
            .classList.remove("show-main-modal");
          document.location.reload();
        }
      };
      xhttp.open("POST", "https://lifebank.ng/crm/new/api/expected", true);
      xhttp.setRequestHeader(
        "Content-type",
        "application/x-www-form-urlencoded"
      );
      xhttp.send(
        `id=${editExpectedOrdersId}&amount=${editExpectedOrdersValue}&user=${asignedUser}`
      );
    });

    // Fetch users
    var xhttpAgent = new XMLHttpRequest();
    xhttpAgent.onreadystatechange = function() {
      if (
        this.readyState == 1 ||
        this.readyState == 2 ||
        this.readyState == 3
      ) {
        document
          .querySelector(".loader-container")
          .classList.add("show-loader");
      } else if (this.readyState == 4 && this.status == 200) {
        var resData = JSON.parse(xhttpAgent.responseText).sort((a, b) =>
          a.username.toUpperCase() > b.username.toUpperCase() ? 1 : -1
        );
        // var resData = JSON.parse(xhttpAgent.responseText);
        // console(resData);
        document
          .querySelector(".loader-container")
          .classList.remove("show-loader");
        //  Build HTML
        document.querySelector("#asigned-list").innerHTML =
          '<option value="">Assign Agent</option>' +
          resData
            .map(data => {
              return `<option value="${data.id}">${data.username}</option>`;
            })
            .join("");
        document.querySelector('#asigned-list').value
      }
    };
    xhttpAgent.open(
      "GET",
      "https://lifebank.ng/crm/new/api/user/getlist/all",
      true
    );
    xhttpAgent.send();

    // Clear dialog for update
    window.addEventListener("click", e => {
      if (e.target.id == "edit-modal") {
        document
          .querySelector("#edit-modal")
          .classList.remove("show-main-modal");
      }
    });

    // Validate form
    window.addEventListener("keyup", validateQueryForm);
    // Clear query off screen
    document.querySelector("#cancel-btn").addEventListener("click", e => {
      document.querySelector(".main-modal").classList.remove("show-main-modal");
    });
    // Clear query off screen
    window.addEventListener("click", e => {
      if (e.target.id == "modal") {
        document
          .querySelector(".main-modal")
          .classList.remove("show-main-modal");
      }
    });
  }
};
xhttp.open("GET", `https://lifebank.ng/crm/new/api/hospital/edit`, true);
xhttp.send();


document.querySelector("#asigned-list").addEventListener('change', e => {
  asignedUser = e.target.value;
});

// Nav Links
document.querySelector('.all-issues-link').addEventListener('click', e => {
  e.preventDefault();
  window.location.href = './all_issues.html';
});

document.querySelector('.minimum-orders-link').addEventListener('click', e => {
  e.preventDefault();
  window.location.href = './minimum-orders.html';
});