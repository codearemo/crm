var url = localStorage.getItem('url');

var auth_details = JSON.parse(localStorage.getItem('crmAuth'));
var paginationList = [];
var paginationQueries;

if (
  localStorage.getItem('crmAuth') &&
  (auth_details.class != 'CustomerCare' || auth_details.class == 'All')
) {
  document.querySelector('.login-session').innerHTML = `<p><em>${
    auth_details.username
  }</em> logout</p>`;
  document.querySelector('.user-nav-info h5').innerText = auth_details.username;
  document.querySelector('.user-nav-info p').innerText = auth_details.email;

  document.querySelector('#drop-down-hold').innerHTML = `${
    auth_details.username
  } <i class="far fa-user"></i> <i class="fas fas fa-ellipsis-v"></i>`;

  function populatePieChart(piechart) {
    return piechart.map(piechartSection => {
      return [piechartSection.title, parseInt(piechartSection.amount)];
    });
  }

  // Get queries request
  var xhttpAllIssues = new XMLHttpRequest();
  xhttpAllIssues.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      document
        .querySelector('.loader-container')
        .classList.remove('show-loader');
      // Sort the issues based on dept
      var resData = xhttpAllIssues.responseText;
      if (resData == '"No Issues"') {
        document.querySelector(
          '#queries-hold'
        ).innerHTML = `<h3>${resData}</h3>`;
        return;
      }
      resData = JSON.parse(xhttpAllIssues.responseText);
      var queries = resData.sort((a, b) => (a.id > b.id ? -1 : 1));

      // Build individual cardlike query
      paginationQueries = buildQueryList(queries);

      // Cal. number of pages needed for pagination
      var numOfPages = Math.ceil(paginationQueries.length / 10);

      // Build pagination indicator at page bottom
      for (var i = 1; i <= numOfPages; i++) {
        if (i == 1) {
          paginationList.push(`<li class="active"><a href="#">${i}</a></li>`);
        } else {
          paginationList.push(`<li><a href="#">${i}</a></li>`);
        }
      }

      // Populate data into the HTML
      document.querySelector(
        '#queries-hold'
      ).innerHTML = `<div class="row head-section">
                      <div class="col-sm-offset-1 col-sm-3">
                        <h3>All Resolved Queries</h3>
                      </div>
                        <input class="col-sm-offset-4 col-sm-3 search_input" type="text" placeholder="Search Hospital">
                    </div>
                    <br>
                    <div class="query_list">
                      ${paginationQueries.splice(0, 10).join('')}
                    </div>`;
      // Populate the pagination
      document.querySelector('.pagination').innerHTML = paginationList.join(
        ' '
      );

      // Select page
      document.querySelectorAll('.pagination li').forEach(paginationItem => {
        paginationItem.addEventListener('click', e => {
          runPagination(e);
        });
      });

      // Search for query
      document.querySelector('.search_input').addEventListener('keyup', e => {
        // queries.forEach(query => console.dir(query));
        paginationQueries = resData.filter(query =>
          query.name.toLowerCase().includes(e.target.value.toLowerCase())
        );

        if (paginationQueries.length == 0) {
          document.querySelector(
            '.query_list'
          ).innerHTML = `<h3>No Matching Item</h3>`;
          return;
        } else {
          paginationQueries = buildQueryList(paginationQueries);

          document.querySelector(
            '.query_list'
          ).innerHTML = `${paginationQueries.splice(0, 10).join('')}`;
        }
      });
    }
  };
  xhttpAllIssues.open(
    'GET',
    `https://lifebank.ng/crm/new/api/issues/admin/get`,
    true
  );
  xhttpAllIssues.send();

  function runPagination(e) {
    var currentPage = e.target.innerText;
    document.querySelectorAll('.pagination li').forEach(pageIndicator => {
      pageIndicator.classList.remove('active');
      if (e.target.innerText == pageIndicator.innerText) {
        pageIndicator.classList.add('active');
      }
    });
    document.querySelector(
      '#queries-hold'
    ).innerHTML = `<h3 class="col-sm-offset-2">All Queries</h3><br> ${[
      ...paginationQueries
    ]
      .splice(10 * (currentPage - 1), 10)
      .join(' ')}`;
  }

  function formDate(dateFormat) {
    var queryDate = new Date(dateFormat * 1000);
    return `${addZero(queryDate.getDay() + 1)}-${addZero(
      queryDate.getMonth() + 1
    )}-${queryDate.getFullYear()}   ${addZero(queryDate.getHours())}:${addZero(
      queryDate.getMinutes()
    )}:${addZero(queryDate.getSeconds())}`;
  }

  function addZero(value) {
    if (value < 10) {
      return `0${value}`;
    } else {
      return value;
    }
  }

  function buildQueryList(queries) {
    return queries.map(query => {
      return `
      <div class="card w-75">
        <div class="card-body">
          <h5 class="card-title">${query.title}</h5>
          <em>${query.name} - <strong>${query.class}</strong></em><br>
          <em class="time_section">${formDate(
            parseInt(query.tym)
          )}</strong></em><br><br>
          <p>${query.issues}</p>
          <div class="action_btn_hold">
          <a class="expand-action" href="#" class="btn" data-toggle="collapse" data-target="#collapseExample${
            query.id
          }" aria-expanded="false" aria-controls="collapseExample">More</a>
          </div>
        </div>
        
        <div class="collapse more_query" id="collapseExample${query.id}">
          <h5>Steps taken:</h5>
          <p>${query.step}</p>
        </div>
      </div>
      `;
    });
  }

  // Nav Links
  document.querySelector('.all-issues-link').addEventListener('click', e => {
    e.preventDefault();
    window.location.href = './all_issues.html';
  });

  document
    .querySelector('.minimum-orders-link')
    .addEventListener('click', e => {
      e.preventDefault();
      window.location.href = './minimum-orders.html';
    });

  document.querySelector('#drop-down-hold').addEventListener('click', () => {
    document.querySelector('.overlay').style.display = 'block';
    setTimeout(() => {
      document.querySelector('nav').style.transform = 'translateX(0%)';
    }, 100);
  });

  document.querySelector('.close-nav').addEventListener('click', () => {
    document.querySelector('.overlay').style.display = 'none';
    setTimeout(() => {
      document.querySelector('nav').style.transform = 'translateX(100%)';
    }, 100);
  });

  document.querySelector('.logout-item').addEventListener('click', () => {
    localStorage.removeItem('crmAuth');
    window.location.replace(`${url}`);
  });
} else {
  window.location.replace(`${url}`);
}
