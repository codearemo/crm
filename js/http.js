// Importing SVG
import { mySVG } from './renderer.js';

var url = localStorage.getItem('url');
var auth_details = JSON.parse(localStorage.getItem('crmAuth'));

var editExpectedOrdersId, editExpectedOrdersValue;
var issueDeptSubmission, issueTitleSubmission, Hospital_ID;

var numOfFailedHospitals = document.querySelector('#failedHospitals');
var numOfPassedHospitals = document.querySelector('#passsedHospitals');
var numOfIssues = document.querySelector('#numOfIssues');
var numberOfOrdersToday = document.querySelector('#numberOfOrdersToday');
var numOfMonthOrders = document.querySelector('#numOfMonthOrders');
var ordersPerDay = document.querySelector('#ordersPerDay');
var submitButton = document.querySelector('.submit-issue');
var queryContent = document.querySelector('.query-content');
var userInfoHold = document.querySelector('.user-nav-info h5');
var userEmail = document.querySelector('.user-nav-info p');

// Blanck page
function blanckPage(text) {
  return `
  <div>
  ${mySVG}
  <p>No ${text}<p>
  </div>
  `;
}

function loadXMLDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      document
        .querySelector('.loader-container')
        .classList.remove('show-loader');

      var resData = JSON.parse(this.responseText);
      // console.log(resData);

      // Dashboard
      numOfFailedHospitals.innerHTML = resData[0].Failed;
      numOfPassedHospitals.innerHTML = resData[0].Passed;
      numOfIssues.innerHTML = resData[0].issues;
      numberOfOrdersToday.innerHTML =
        resData[0].Total == null ? 0 : resData[0].Total;
      numOfMonthOrders.innerHTML = resData[0].Active;
      ordersPerDay.innerHTML = resData[0].assigned;

      document.querySelector('.login-session').innerHTML = `<span>${
        auth_details.username
      }</span>
      <i class="far fa-user"></i> <i class="fas fas fa-ellipsis-v"></i>`;

      // Change password
      document
        .querySelector('.change-password')
        .addEventListener('click', () => {
          window.location.href = './update-password.html';
        });

      // Link to Complaint Form
      document
        .querySelector('.complaint-link')
        .addEventListener('click', () => {
          window.location.href = './complaint.html';
        });

      // Logout
      document.querySelector('.logout-item').addEventListener('click', () => {
        localStorage.removeItem('crmAuth');
        window.location.replace(`${url}`);
      });

      document
        .querySelector('#drop-down-hold')
        .addEventListener('click', () => {
          document.querySelector('.overlay').style.display = 'block';
          setTimeout(() => {
            document.querySelector('nav').style.transform = 'translateX(0%)';
          }, 100);
        });

      document.querySelector('.close-nav').addEventListener('click', () => {
        document.querySelector('.overlay').style.display = 'none';
        setTimeout(() => {
          document.querySelector('nav').style.transform = 'translateX(100%)';
        }, 100);
      });

      userInfoHold.innerHTML = auth_details.username;
      userEmail.innerHTML = auth_details.email;

      // Hospital Section
      var hospitalsSection = resData[1]
        .sort((a, b) => (a.name > b.name ? 1 : -1))
        .map((hospital, index) => {
          return `
          <tr>
            <th scope="row">${index + 1}</th>
            <td>${hospital.name}</td>
            <td>${hospital.Expected}</td>
          <!--  <td> <button type="button" id="${
            hospital.id
          }" class="btn btn-primary btn-sm edit-expected-orders">Edit</button></td> -->
          </tr>`;
        });

      // Fill the table
      document.querySelector('#table-section').innerHTML = `
      <thead>
      <tr>
      <th scope="col">#</th>
      <th scope="col">Hospitals</th>
      <th scope="col">Expected Orders/month</th>
    <!--  <th scope="col">Edit Expected Orders</th> -->
      </tr>
      ${hospitalsSection.join('')}
      </thead>
      <tbody>
      
      <caption>List of hospitals and their respective orders</caption>
      `;

      // Click edit button for for new order update
      document
        .querySelectorAll('.edit-expected-orders')
        .forEach(hospitalEdit => {
          hospitalEdit.addEventListener('click', e => {
            document.querySelector(
              '#edit-modal .alert-dialog h4 span'
            ).innerHTML = e.path[2].children[1].innerText;
            document.querySelector('#update_number').value =
              e.path[2].children[2].innerText;
            document
              .querySelector('#edit-modal')
              .classList.add('show-main-modal');
            editExpectedOrdersId = e.target.id;
          });
        });

      // Update Expected Amount of Orders
      document.querySelector('#updateBotton').addEventListener('click', e => {
        e.preventDefault();
        editExpectedOrdersValue = document.querySelector('.edit-form input')
          .value;
        // POST request
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (
            this.readyState == 1 ||
            this.readyState == 2 ||
            this.readyState == 3
          ) {
            document
              .querySelector('.loader-container')
              .classList.add('show-loader');
          } else if (this.readyState == 4 && this.status == 200) {
            alert(this.responseText);
            document
              .querySelector('#edit-modal')
              .classList.remove('show-main-modal');
            document.location.reload();
          }
        };
        xhttp.open('POST', 'https://lifebank.ng/crm/new/api/expected', true);
        xhttp.setRequestHeader(
          'Content-type',
          'application/x-www-form-urlencoded'
        );
        xhttp.send(
          `id=${editExpectedOrdersId}&amount=${editExpectedOrdersValue}`
        );
      });

      // Clear dialog for update
      window.addEventListener('click', e => {
        if (e.target.id == 'edit-modal') {
          document
            .querySelector('#edit-modal')
            .classList.remove('show-main-modal');
        }
      });
      // Activity Section
      var hospitalsNeedingAttention = resData[2]
        .sort((a, b) => (a.Hospital_name > b.Hospital_name ? 1 : -1))
        .map(hospital => {
          return `
        <div>
          <div>
            <p class="card-hospital-name">${hospital.Hospital_name}</p>
          </div>
          <p>${hospital.monthly}/${hospital.min}</p>
          <p>${hospital.Hospital_phone}</p>
          <div class="actions-btn-hold">
            <button type="button" id=${
              hospital.Hospital_id
            } class="btn btn-primary btn-sm tasks">Create Issue</button>
          </div>
        </div>
        `;
        });

      document.querySelector('.hospital-info-hold').innerHTML =
        resData[2].length == 0
          ? blanckPage('Activity')
          : hospitalsNeedingAttention.join(' ');

      // Make call action
      document.querySelectorAll('.tasks').forEach(task => {
        task.addEventListener('click', e => {
          Hospital_ID = e.target.id;
          document
            .querySelector('.main-modal')
            .classList.add('show-main-modal');
          document
            .querySelector('.alert-dialog')
            .classList.add('show-alert-dialog');
        });
      });
      // Validate form
      window.addEventListener('keyup', validateQueryForm);
      // Clear query off screen
      document.querySelector('#cancel-btn').addEventListener('click', e => {
        document
          .querySelector('.main-modal')
          .classList.remove('show-main-modal');
      });
      // Clear query off screen
      window.addEventListener('click', e => {
        if (e.target.id == 'modal') {
          document
            .querySelector('.main-modal')
            .classList.remove('show-main-modal');
        }
      });

      // Select Title for Issue Creation
      document
        .querySelector('#title-categories')
        .addEventListener('change', () => {
          issueTitleSubmission = document.querySelector('#title-categories')
            .value;
          switch (issueTitleSubmission) {
            case 'Price':
              issueDeptSubmission = 'sales';
              break;
            case 'Time delay':
              issueDeptSubmission = 'operations';
              break;
            case 'Unfulfilled orders':
              issueDeptSubmission = 'operations';
              break;
            case 'Invoice error':
              issueDeptSubmission = 'finance';
              break;
            case 'Bad blood':
              issueDeptSubmission = 'operations';
              break;
            case 'Debt':
              issueDeptSubmission = 'finance';
              break;
            case 'Gifts Rewards':
              issueDeptSubmission = 'operations';
              break;
            case 'Rude Riders':
              issueDeptSubmission = 'operations';
              break;
            case 'Lost contact':
              issueDeptSubmission = 'operations';
              break;
            case 'Change in hospital personnel':
              issueDeptSubmission = 'sales';
              break;
            case 'Low cause of switching':
              issueDeptSubmission = 'operations';
              break;
            case 'Previous commitment':
              issueDeptSubmission = 'sales';
              break;
            case 'Rude Slow Customer Service(Sense of urgency)':
              issueDeptSubmission = 'operations';
              break;
            case 'Market Downturn':
              issueDeptSubmission = 'sales';
              break;
          }
          validateQueryForm();
        });

      // Submit Issue
      submitButton.addEventListener('click', e => {
        e.preventDefault();
        // POST Issues request
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (
            this.readyState == 1 ||
            this.readyState == 2 ||
            this.readyState == 3
          ) {
            document
              .querySelector('.loader-container')
              .classList.add('show-loader');
          } else if (this.readyState == 4 && this.status == 200) {
            document
              .querySelector('.main-modal')
              .classList.remove('show-main-modal');
            alert(this.responseText);
            window.location.reload();
          }
        };
        xhttp.open('POST', 'https://lifebank.ng/crm/new/api/issues/log', true);
        xhttp.setRequestHeader(
          'Content-type',
          'application/x-www-form-urlencoded'
        );
        xhttp.send(
          `class=${issueDeptSubmission}&title=${issueTitleSubmission}&Hospital_ID=${Hospital_ID}&query=${
            document.querySelector('.issue-form textarea').value
          }`
        );
      });
    }
  };
  xhttp.open(
    'GET',
    `https://lifebank.ng/crm/new/api/dashboard/personalized/${auth_details.id}`,
    true
  );
  xhttp.send();
}

function validateQueryForm() {
  if (
    queryContent.value.trim().length == 0 ||
    issueDeptSubmission == undefined ||
    issueTitleSubmission == undefined
  ) {
    submitButton.setAttribute('disabled', '');
    submitButton.classList.add('disabled-botton');
  } else {
    submitButton.removeAttribute('disabled');
    submitButton.classList.remove('disabled-botton');
  }
}

if (localStorage.getItem('crmAuth') && auth_details.class == 'CustomerCare') {
  loadXMLDoc();
  validateQueryForm();
} else {
  window.location.replace(`${url}`);
}
