var url = localStorage.getItem('url');
// Auth details form LocalStorage
var auth_details = JSON.parse(localStorage.getItem('crmAuth'));
var queryCounter, piechart, barchart, newColour;

function loadXMLDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 1 || this.readyState == 2 || this.readyState == 3) {
      document.querySelector('.loader-container').classList.add('show-loader');
    } else if (this.readyState == 4 && this.status == 200) {
      var resData = JSON.parse(this.responseText);
      queryCounter = resData[0];
      barchart = resData[1];
      piechart = resData[2];

      // Populate the queries counter
      document.querySelector('.cards_hold').innerHTML = `
      <div class="dashboard_card">
        <h4>Unresolved Queries</h4>
        <hr>
        <h5>${queryCounter.Unresolved}</h5>
      </div>
      <div class="dashboard_card">
        <h4>Resolved Queries</h4>
        <hr>
        <h5>${queryCounter.Resolved}</h5>
      </div>
      <div class="dashboard_card">
        <h4>Total Queries</h4>
        <hr>
        <h5>${parseInt(queryCounter.Resolved) +
          parseInt(queryCounter.Unresolved)}</h5>
      </div>
      `;

      document.querySelector('#drop-down-hold').innerHTML = `${
        auth_details.username
      } <i class="far fa-user"></i> <i class="fas fas fa-ellipsis-v"></i>`;

      document.querySelector('.user-nav-info h5').innerText =
        auth_details.username;
      document.querySelector('.user-nav-info p').innerText = auth_details.email;

      function populatePieChart(piechart) {
        return piechart.map(piechartSection => {
          return [piechartSection.title, parseInt(piechartSection.amount)];
        });
      }

      function populateBarChart(barchart) {
        return barchart.map(barchartSection => {
          return [
            barchartSection.class,
            parseInt(barchartSection.amount),
            generateRandomColors()
          ];
        });
      }

      function generateRandomColors() {
        for (var i = 0; i < 7; i++) {
          newColour = '#' + ((Math.random() * 0xffffff) << 0).toString(16);
        }
        return newColour;
      }

      // For PieChart
      google.charts.load('current', { packages: ['corechart'] });
      google.charts.setOnLoadCallback(drawPieChart);
      function drawPieChart() {
        if (piechart.length == 0) {
          document.getElementById('chartContainer_pie_chart').innerHTML =
            '<h3>No Data</h3>';
        } else {
          var data = google.visualization.arrayToDataTable([
            ['Task', 'Frequency of query'],
            ...populatePieChart(piechart)
          ]);

          var options = {
            title: 'Query Distribution',
            pieHole: 0.0
            // pieSliceText: 'label'
          };

          var chartContainer_pie_chart = new google.visualization.PieChart(
            document.getElementById('chartContainer_pie_chart')
          );
          chartContainer_pie_chart.draw(data, options);
        }
      }

      // For BarChart
      google.charts.load('current', { packages: ['corechart'] });
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        if (barchart.length == 0) {
          document.getElementById('chartContainer_bar_chart').innerHTML =
            '<h3>No Data</h3>';
        } else {
          var data = google.visualization.arrayToDataTable([
            ['Departments', 'Amount', { role: 'style' }],
            ...populateBarChart(barchart)
          ]);

          var view = new google.visualization.DataView(data);
          view.setColumns([
            0,
            1,
            {
              calc: 'stringify',
              sourceColumn: 1,
              type: 'string',
              role: 'annotation'
            },
            2
          ]);

          var options = {
            title: 'Query Cummulation',
            height: 500,
            bar: { groupWidth: '95%' },
            legend: { position: 'none' }
          };
          var chartContainer_bar_chart = new google.visualization.ColumnChart(
            document.getElementById('chartContainer_bar_chart')
          );
          chartContainer_bar_chart.draw(view, options);
        }
      }

      document
        .querySelector('.loader-container')
        .classList.remove('show-loader');
    }
  };
  xhttp.open('GET', 'https://lifebank.ng/crm/new/api/dashboard/admin', true);
  xhttp.send();
}

if (
  localStorage.getItem('crmAuth') &&
  (auth_details.class == 'All' || auth_details.class == 'tech')
) {
  loadXMLDoc();
} else {
  window.location.replace(`${url}`);
}

// Nav Links
document.querySelector('.all-issues-link').addEventListener('click', e => {
  e.preventDefault();
  window.location.href = './all_issues.html';
});

document.querySelector('.minimum-orders-link').addEventListener('click', e => {
  e.preventDefault();
  window.location.href = './minimum-orders.html';
});

document.querySelector('#drop-down-hold').addEventListener('click', () => {
  document.querySelector('.overlay').style.display = 'block';
  setTimeout(() => {
    document.querySelector('nav').style.transform = 'translateX(0%)';
  }, 100);
});

document.querySelector('.close-nav').addEventListener('click', () => {
  document.querySelector('.overlay').style.display = 'none';
  setTimeout(() => {
    document.querySelector('nav').style.transform = 'translateX(100%)';
  }, 100);
});

document.querySelector('.logout-item').addEventListener('click', () => {
  localStorage.removeItem('crmAuth');
  window.location.replace(`${url}`);
});
